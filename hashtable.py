import numpy as np
from collision import *

class HashTable(dict):
    def __init__(self, n, l):
        super()
        self.n = n # hash table size
        self.l = l # 3D grid size

    def hash(self, ijk):
        # ijk: grid coordinates of the vertex
        return (ijk[0] * 73856093 ^ ijk[1] * 19349663 ^ ijk[2] * 83492791) % self.n

    def get_index(self, x):
        ''' 
            Get hash index in hashtable of a 3D position
        '''
        # x: world coordinates of the vertex
        ijk = np.floor(x / self.l).astype("int64")
        idx = self.hash(ijk)
        return idx

    def add_vertex(self, x, i, j):
        '''
            Insert a 3D point with index (i, j) to hashtable
        '''
        # compute hash index
        idx = self.get_index(x)
        if idx in self:
            self[idx].append((i, j))
        else:
            self[idx] = [(i, j)]

    def get_cells(self, p0, p1, p2):
        '''
            return a list of cells bounding p0, p1, p2, each of the cell is represented by top-left position
        '''
        i_min, i_max, j_min, j_max, k_min, k_max = get_aabb(p0, p1, p2)
        i = i_min
        j = j_min
        k = k_min
        cells = []

        # NOTE: to parallize
        while i <= i_max:
            while j <= j_max:
                while k <= k_max:
                    cells.append(np.array([i, j, k]))
                    k = k + self.l
                k = k_min
                j = j + self.l
            j = j_min
            i = i + self.l
        return cells

    def get_points(self, p0, p1, p2):
        cells = self.get_cells(p0, p1, p2)
        indices = []
        for c in cells:
            indices.append(self.get_index(c))
        pts = []
        for idx in indices:
            if idx in self:
                for p in self[idx]:
                    pts.append(p)
        
        return pts
