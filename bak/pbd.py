from constriants import StretchCON
import taichi as ti
from random import random
from tqdm import tqdm
from math import sin, pi, sqrt

from utils import * 
from simulate import *

#init gui
ti.init(arch=ti.gpu)

gui = ti.GUI(background_color=0x222222)

dtype = "float32"
#init grid points 
top_left = np.array([0,0,0], dtype = dtype)
h = np.array([1,0,0], dtype = dtype)
w = np.array([0,1,0],  dtype = dtype)
dim_x = 6
dim_y = 6
l_x = 0.2
l_y = 0.2

grid = init_grid(top_left, h, w, dim_x, dim_y)
grid_next = grid.copy()
v = np.zeros_like(grid)
v_next = np.zeros_like(grid)
a = np.zeros_like(grid)
g = -9.8
a[:,:, 1] = g * np.ones_like(a[:,:, 1])

# grid = convert_np_to_taichi_vec(grid)
# grid_next = convert_np_to_taichi_vec(grid_next)
# v = convert_np_to_taichi_vec(v)
# v_next = convert_np_to_taichi_vec(v_next)
# a = convert_np_to_taichi_vec(a)


constraints = []

for i in range(dim_x - 1):
    for j in range(dim_y):
        if i == 0 and j == 0:
            constraints.append(StretchCON((i, j), (i + 1, j), 0, 1, l_x, 1))
        else: 
            constraints.append(StretchCON((i, j), (i + 1, j), 1, 1, l_x, 1))

for i in range(dim_x):
    for j in range(dim_y - 1):
        if i == 0 and j == 0:
            constraints.append(StretchCON((i, j), (i, j + 1), 0, 1, l_y, 1))
        else: 
            constraints.append(StretchCON((i, j), (i, j + 1), 1, 1, l_y, 1))       

for i in range(dim_x -1):
    for j in range(dim_y - 1):
        if i == 0 and j == 0:
            constraints.append(StretchCON((i, j), (i + 1, j + 1), 0, 1, np.sqrt(2) * l_y, 1))
        else: 
            constraints.append(StretchCON((i, j), (i + 1, j + 1), 1, 1, np.sqrt(2) * l_y, 1))      

# for i in range(dim_x -1):
#     for j in range(dim_y - 1):
#         constraints.append(StretchCON((i, j + 1), (i + 1, j), 1, 1, np.sqrt(2) * l_y, 1))    



t = 1/60



for i in range(10000):
    v_next = v_forward(v_next, v, a, t)

    grid_next = x_forward_verlet(grid_next, grid, t, v, v_next)
    fix_point(grid_next)

    itr = 2
    for i in range(itr):
        for c in constraints:
            c.project(grid_next)


    v = v_update(v, grid_next, grid, t)

    grid = x_update(grid, grid_next)
    visualize_grid(grid, gui)
    gui.show()