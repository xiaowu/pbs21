# examples/Python/Basic/mesh.py

from constriants import StretchCON
import taichi as ti
from random import random
from tqdm import tqdm
from math import sin, pi, sqrt
import open3d as o3d

import copy

from utils import * 
from simulate import *



top_left = np.array([0,0,0], dtype = dtype)
h = np.array([1,0,0], dtype = dtype)
w = np.array([0,1,0],  dtype = dtype)
dim_x = 11
dim_y = 11
total_vert = dim_x * dim_y
l_x = 0.1
l_y = 0.1

grid = init_grid(top_left, h, w, dim_x, dim_y)
verts = grid.reshape(total_vert, 3)
faces = init_faces(dim_x, dim_y)

mesh = o3d.geometry.TriangleMesh()

mesh.vertices = o3d.utility.Vector3dVector(verts)

mesh.triangles = o3d.utility.Vector3iVector(faces)

print("Computing normal and rendering it.")
mesh.compute_vertex_normals()



grid_next = grid.copy()
v = np.zeros_like(grid)
v_next = np.zeros_like(grid)
a = np.zeros_like(grid)
g = -9.8
a[:,:, 1] = g * np.ones_like(a[:,:, 1])




constraints = []

for i in range(dim_x - 1):
    for j in range(dim_y):
        if i == 0 and j == 0:
            constraints.append(StretchCON((i, j), (i + 1, j), 0, 1, l_x, 0.9))
        else: 
            constraints.append(StretchCON((i, j), (i + 1, j), 1, 1, l_x, 0.9))

for i in range(dim_x):
    for j in range(dim_y - 1):
        if i == 0 and j == 0:
            constraints.append(StretchCON((i, j), (i, j + 1), 0, 1, l_y, 0.9))
        else: 
            constraints.append(StretchCON((i, j), (i, j + 1), 1, 1, l_y, 0.9))       

for i in range(dim_x -1):
    for j in range(dim_y - 1):
        if i == 0 and j == 0:
            constraints.append(StretchCON((i, j), (i + 1, j + 1), 0, 1, np.sqrt(2) * l_y, 0.9))
        else: 
            constraints.append(StretchCON((i, j), (i + 1, j + 1), 1, 1, np.sqrt(2) * l_y, 0.9))      

# for i in range(dim_x -1):
#     for j in range(dim_y - 1):
#         constraints.append(StretchCON((i, j + 1), (i + 1, j), 1, 1, np.sqrt(2) * l_y, 1))    



t = 1/60



vis = o3d.visualization.VisualizerWithKeyCallback()


def change_ef_dir(vis):
    ef_x = 5 * np.random.random()
    ef_y = 3 * np.random.random()
    ef_z = 5 * np.random.random()
    a[:,:, 1] = (g + ef_y) * np.ones_like(a[:,:, 1])
    a[:,:, 0] = ef_x * np.ones_like(a[:,:, 1])
    a[:,:, 2] = ef_z * np.ones_like(a[:,:, 1])

vis.register_key_callback(ord("5"), change_ef_dir)

vis.create_window()


vis.reset_view_point(True)
vis.add_geometry(mesh)



coordinate_frame = o3d.geometry.TriangleMesh.create_coordinate_frame(
    size=1, origin=[0, 0, 0]
)
vis.add_geometry(coordinate_frame)  # coordinate frame


while True:
    v_next = v_forward(v_next, v, a, t)

    grid_next = x_forward_verlet(grid_next, grid, t, v, v_next)
    fix_point(grid_next)

    itr = 6
    for i in range(itr):
        for c in constraints:
            c.project(grid_next)

    

    v = v_update(v, grid_next, grid, t)

    grid = x_update(grid, grid_next)

    verts = grid.reshape(total_vert, 3)

    mesh.vertices = o3d.utility.Vector3dVector(verts)
    mesh.compute_vertex_normals()
    vis.update_geometry(mesh)
    if not vis.poll_events():
        break
    vis.update_renderer()