import numpy as np
import math

def dist(p1, p2):
    return np.sqrt(np.dot(p1 - p2, p1 - p2))

def normalize(p1):
    norm = np.linalg.norm(p1)
    if norm < 1e-16:
        return p1
    return p1 / norm
    
def not_zero(x):
    eps = 1e-6
    return abs(x - eps) > eps

def is_zero(x):
    return not not_zero(x)

class StretchCON:
    #implements the constraint C = |x_0 - x_1| - l
    def __init__(self, p1, p2, w1, w2, l, k) -> None:
        self.p1 = p1
        self.p2 = p2
        self.w1 = w1
        self.w2 = w2
        self.l = l
        self.k = k
    
    def C(self, p1, p2):
        return dist(p1, p2) - self.l
    
    def project(self, grid):
        p1 = grid[self.p1]
        p2 = grid[self.p2]
        s = 1/(self.w1 + self.w2) * (self.C(p1, p2))
        del_p1 =  -self.w1/(self.w1 + self.w2) * s * (p1 - p2)/dist(p1, p2)
        del_p2 =  self.w2/(self.w1 + self.w2) * s * (p1 - p2)/dist(p1, p2)
        grid[self.p1] = p1 + self.k * del_p1
        grid[self.p2] = p2 + self.k * del_p2
    

class BendCon:
    # Derivatives from Bridson, Simulation of Clothing with Folds and Wrinkles
	# his modes correspond to the derivatives of the bending angle arccos(n1 dot n2) with correct scaling
    def __init__(self, p0, p1, p2, p3, w0, w1, w2, w3, restAngle, k):
        self.p0, self.p1, self.p2, self.p3 = p0, p1, p2, p3
        self.w0, self.w1, self.w2, self.w3 = w0, w1, w2, w3
        self.restAngle = restAngle
        self.k = k

    def project(self, grid):
        p0, p1, p2, p3 = grid[self.p0], grid[self.p1], grid[self.p2], grid[self.p3]
        if self.w0 == 0. or self.w1 == 0.:
            return False
        e = p3 - p2
        elen = np.linalg.norm(e)
        if is_zero(elen):
            return False
        inv_elen = 1. / elen
        n1 = np.cross((p2 - p0), (p3 - p0))
        n1 = normalize(n1)
        n2 = np.cross((p3 - p1), (p2 - p1))
        n2 = normalize(n2)
        
        d0 = elen * n1
        d1 = elen * n2
        d2 = (p0 - p3).dot(e) * inv_elen * n1 + (p1 - p3).dot(e) * inv_elen * n2
        d3 = (p2 - p0).dot(e) * inv_elen * n1 + (p2 - p1).dot(e) * inv_elen * n2

        dot = n1.dot(n2)
        dot = max(-1.0, dot)
        dot = min(1.0, dot)
        phi = math.acos(dot)

        lamb = self.w0 * np.linalg.norm(d0) + \
            self.w1 * np.linalg.norm(d1) + \
            self.w2 * np.linalg.norm(d2) + \
            self.w3 * np.linalg.norm(d3)
        if lamb == 0.:
            return False
        lamb = ((phi - self.restAngle) / lamb) * self.k
        if np.cross(n1, n2).dot(e) > 0.:
            lamb = -lamb
        corr0 = -self.w0 * lamb * d0
        corr1 = -self.w1 * lamb * d1
        corr2 = -self.w2 * lamb * d2
        corr3 = -self.w3 * lamb * d3

        grid[self.p0] += corr0
        grid[self.p1] += corr1
        grid[self.p2] += corr2
        grid[self.p3] += corr3

        return True
        
def calc_barycentric_coords(p, p0, p1, p2):
    '''
        find barycentric coordinates of closest point on triangle
    '''
    b0, b1, b2 = 1. / 3., 1. / 3., 1. / 3.

    d1 = p1 - p0
    d2 = p2 - p0
    pp0 = p - p0
    a = np.dot(d1, d1)
    b = np.dot(d2, d1)
    c = np.dot(pp0, d1)
    d = b
    e = np.dot(d2, d2)
    f = np.dot(pp0, d2)
    det = a*e - b*d # NOTE: 0-divide q

    if not_zero(det):
        s = (c*e - b*f) / det
        t = (a*f - c*d) / det
        b0 = 1 - s - t
        b1 = s
        b2 = t

        if (b0 < 0.):
            dist = p2 - p1
            if is_zero(np.dot(dist, dist)):
                t = 0.5
            else:
                t = np.dot(dist, (p - p1)) / np.dot(dist, dist)   
            t = np.clip(t, 0, 1)
            b0 = 0.
            b1 = 1 - t
            b2 = t
        elif (b1 < 0.):
            dist = p0 - p2
            if is_zero(np.dot(dist, dist)):
                t = 0.5
            else:
                t = np.dot(dist, (p - p2)) / np.dot(dist, dist)
            t = np.clip(t, 0, 1)
            b1 = 0.
            b2 = 1 - t
            b0 = t
        elif (b2 < 0.):
            dist = p1 - p0
            if is_zero(np.dot(dist, dist)):
                t = 0.5
            else:
                t = np.dot(dist, (p - p0)) / np.dot(dist, dist)
            t = np.clip(t, 0, 1)
            b2 = 0.
            b0 = 1. - t
            b1 = t     
    return b0, b1, b2


class CollisionCON:
    # implements collision constraints
    def __init__(self, p, p0, p1, p2, w, l, k):
        self.p = p
        self.p0 = p0
        self.p1 = p1
        self.p2 = p2
        self.w = w[p]
        self.w0 = w[p0]
        self.w1 = w[p1]
        self.w2 = w[p2]
        self.l = l # cloch thickness
        self.k = k
    
    def project(self, grid):
        compression_stiffness = self.k
        stretcch_stiffness = self.k
        p = grid[self.p]
        p0 = grid[self.p0]
        p1 = grid[self.p1]
        p2 = grid[self.p2]
        w = self.w
        w0 = self.w0
        w1 = self.w1
        w2 = self.w2

        b0, b1, b2 = calc_barycentric_coords(p, p0, p1, p2) 
        pc = p0 * b0 + p1 * b1 + p2 * b2
        n = p - pc
        dist = np.linalg.norm(n)
        # l = np.sqrt(np.dot(n, n))
        c = dist - self.l
        n = normalize(n)

        c_p = n
        c_p0 = -n * b0  
        c_p1 = -n * b1
        c_p2 = -n * b2
        s = w + w0 * b0 * b0 + w1 * b1 * b1 + w2 * b2 * b2

        if is_zero(s):
            return False
        s = c / s
        if c < 0.:
            s *= compression_stiffness
        else:
            return False

        del_p = -s * w * c_p
        del_p0 = -s * w0 * c_p0
        del_p1 = -s * w1 * c_p1
        del_p2 = -s * w2 * c_p2
        grid[self.p] = grid[self.p] + del_p
        grid[self.p0] = grid[self.p0] + del_p0
        grid[self.p1] = grid[self.p1] + del_p1
        grid[self.p2] = grid[self.p2] + del_p2
        return True

