import numpy as np
# from utils import hash
from collision import get_candidate_vertices, penetrating_vertices

def v_forward(v_next, v, a, t):
    v_next = v + a * t
    return v_next

def v_update(v, x_next, x, t):
    v = 2/t  * (x_next - x) - v
    return v

def x_forward_verlet(x_next, x, t, v, v_next):
    x_next = x + t /2 * (v + v_next)
    return x_next

def x_update(x, x_next):
    x = x_next
    return x

def fix_point(x_next, x, fixed_points):
    for fixed_point in fixed_points:
        x_next[tuple(fixed_point)] = x[tuple(fixed_point)]

def collision_detection(hash_table, verts, faces):
    # return: list of pairs of triangle and points that collide
    #[(coordinates of the three vertices, coordinates of the vertices that penetrate the triangle)]
    res = []

    dim_x = faces.shape[0]
    dim_y = faces.shape[1]
    for i in range(dim_x):
        for j in range(dim_y):
            f1 = verts[faces[i, j, 0]]
            f2 = verts[faces[i, j, 1]]
            f1_candidate_points = get_candidate_vertices(hash_table, f1[0], f1[1], f1[2])
            res.append([f1, penetrating_vertices(f1_candidate_points)])
            f2_candidate_points = get_candidate_vertices(hash_table, f2[0], f2[1], f2[2])
            res.append([f2, penetrating_vertices(f2_candidate_points)])

    return res

def generate_collision_constraints():
    # TODO
    # don't forget to test if the vertex enters from below or above
    pass