# examples/Python/Basic/mesh.py

from constriants import StretchCON
import taichi as ti
from random import random
from tqdm import tqdm
from math import sin, pi, sqrt
import open3d as o3d

import copy

from utils import * 
from simulate import *

device = o3d.core.Device("CPU:0")
dtype_f = o3d.core.float32
dtype_i = o3d.core.int32



top_left = np.array([0,0,0], dtype = dtype)
h = np.array([1,0,0], dtype = dtype)
w = np.array([0,-1,0],  dtype = dtype)
dim_x = 6
dim_y = 6
total_vert = dim_x * dim_y
l_x = 0.2
l_y = 0.2

grid = init_grid(top_left, h, w, dim_x, dim_y)

verts = grid.reshape(total_vert, 3)
faces = init_faces(dim_x, dim_y)

# Create an empty triangle mesh
# Use mesh.vertex to access the vertices' attributes
# Use mesh.triangle to access the triangles' attributes
mesh = o3d.geometry.TriangleMesh()

mesh.vertices = o3d.utility.Vector3dVector(verts)

mesh.triangles = o3d.utility.Vector3iVector(faces)
# print(grid.shape)

# print(grid.reshape(total_vert, 3))
# print(grid)
print(faces.shape)
print(faces)
# print("Testing mesh in open3d ...")
# mesh = o3d.io.read_triangle_mesh("/Users/masenov/EECS/PBS/pbd/knot.ply")
print(mesh)
print(np.asarray(mesh.vertices))
print(np.asarray(mesh.triangles))
print("")

print("Try to render a mesh with normals (exist: " +
        str(mesh.has_vertex_normals()) + ") and colors (exist: " +
        str(mesh.has_vertex_colors()) + ")")
o3d.visualization.RenderOption.mesh_show_back_face = True
o3d.visualization.draw_geometries([mesh])
print("A mesh with no normals and no colors does not seem good.")

print("Computing normal and rendering it.")
mesh.compute_vertex_normals()
print(np.asarray(mesh.triangle_normals))
o3d.visualization.draw_geometries([mesh])

print("We make a partial mesh of only the first half triangles.")
mesh1 = copy.deepcopy(mesh)
mesh1.triangles = o3d.utility.Vector3iVector(
    np.asarray(mesh1.triangles)[:len(mesh1.triangles) // 2, :])
mesh1.triangle_normals = o3d.utility.Vector3dVector(
    np.asarray(mesh1.triangle_normals)[:len(mesh1.triangle_normals) //
                                        2, :])
print(mesh1.triangles)
o3d.visualization.draw_geometries([mesh1])

print("Painting the mesh")
mesh1.paint_uniform_color([1, 0.706, 0])
o3d.visualization.draw_geometries([mesh1])