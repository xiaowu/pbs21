import taichi as ti
from random import random
from tqdm import tqdm
from math import sin, pi, sqrt
import numpy as np

dtype = "float32"
def init_grid(top_left, h, w, dim_x, dim_y):
    #return: [dim_x, dim_y, 3]

    x =  np.linspace(0, 1, dim_x, dtype = dtype)
    y = np.linspace(0, 1, dim_y, dtype = dtype)
    xy, yx =  np.meshgrid(x, y)
    return top_left[np.newaxis, np.newaxis,:] + xy[:,:, np.newaxis] * h[np.newaxis, np.newaxis,:] + yx[:,:, np.newaxis] * w[np.newaxis, np.newaxis,:]
    
def init_faces( dim_x, dim_y):
    res = np.zeros(((dim_x - 1), (dim_y - 1), 4, 3))
    for i in range((dim_x - 1)):
        for j in range((dim_y - 1)):
            res[i, j, 0, 0] = get_idx(i, j , dim_x)
            res[i, j, 0, 1] = get_idx(i + 1, j + 1 , dim_x)
            res[i, j, 0, 2] = get_idx(i + 1, j , dim_x)
            res[i, j, 1, 0] = get_idx(i, j , dim_x)
            res[i, j, 1, 1] = get_idx(i, j + 1 , dim_x)
            res[i, j, 1, 2] = get_idx(i + 1, j + 1 , dim_x)

            res[i, j, 2, 0] = get_idx(i, j , dim_x)
            res[i, j, 2, 2] = get_idx(i + 1, j + 1 , dim_x)
            res[i, j, 2, 1] = get_idx(i + 1, j , dim_x)
            res[i, j, 3, 0] = get_idx(i, j , dim_x)
            res[i, j, 3, 2] = get_idx(i, j + 1 , dim_x)
            res[i, j, 3, 1] = get_idx(i + 1, j + 1 , dim_x)
    return res.reshape((dim_x - 1)* (dim_y - 1)* 4, 3)


def get_idx(x, y, dim_x, dim_y = 0):
    return y * dim_x + x
def visualize_grid(grid, gui):
    dim_x = grid.shape[0]
    dim_y = grid.shape[1]
    for i in range(dim_x):
        for j in range(dim_y):
            gui.circle(world2screen(grid[i, j][0],grid[i,j][1]), color=0x00FFFF, radius=5)

def world2screen(x, y):
    L = 1.5
    x = (x-(-L))/(2*L)
    y = (y-(-L))/(2*L)
    return (x,y)

def convert_np_to_taichi_vec(array):
    res = ti.Vector.field(3, ti.f32, shape=(array.shape[:-1]))
    res.from_numpy(array)
    return res


