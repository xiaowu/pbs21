from model import Model
from visualizer import Visualizer
import numpy as np
def main():
    model = Model(setting = '1')
    visuzalizer = Visualizer()
    visuzalizer.add_model(model.mesh)

    def change_ef_dir(vis):
        ef_x = 3 * np.random.random()
        ef_y = 3 * np.random.random()
        ef_z = 3 * np.random.random()
        model.a[:,:, 1] = (model.g + ef_y) * np.ones_like(model.a[:,:, 1])
        model.a[:,:, 0] = ef_x * np.ones_like(model.a[:,:, 1])
        model.a[:,:, 2] = ef_z * np.ones_like(model.a[:,:, 1])

    visuzalizer.register_key(ord("5"), change_ef_dir)
    frame_cnt = 0
    while True:
        print(frame_cnt)
        frame_cnt += 1
        model.update()
        if not visuzalizer.update(model.mesh):
            break

if __name__=='__main__':
    main()
