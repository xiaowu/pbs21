import numpy as np
import open3d as o3d
import os

from constraints import *
from simulate import *
from hashtable import *

class Model:
    def __init__(self, setting = '1'):

        self.setting = setting

        self.dtype = 'float32'
        self.time_step = 1. / 60
        self.frame_cnt = 0

        if self.setting == '1':
            self.dim_x = 21
            self.dim_y = 21
            self.top_left = np.array([0,0,0], dtype = self.dtype)
            self.h = np.array([1,0,0], dtype = self.dtype)
            self.w = np.array([0,1,0],  dtype = self.dtype)
            self.g = -9.8 
            self.k = 0.9
            # bend
            self.iter_bend = 1
            self.init_angle_above = 0.1
            self.init_angle_right = 0
            self.init_angle_below = 0
            self.bend_k = 0.009
            # stretch
            self.itr = 3
            self.colli_k = 1.
            # output
            self.dump_fd = './1/'
            self.dump = False

        self.vert_num = self.dim_x * self.dim_y
        self.lx = 1. / (self.dim_x - 1)
        self.ly = 1. / (self.dim_y - 1)
        self.hash_grid = self.lx * 0.7
        self.ldiag = np.sqrt(self.lx ** 2 + self.ly ** 2)
        self.init()

    def init(self):
        self.init_grid(self.top_left, self.h, self.w, self.dim_x, self.dim_y)
        self.init_fixed_points()
        self.init_w()
        self.init_face_indexes()
        self.init_mesh()
        self.init_v_and_a()
        self.add_constraints()
        
    def init_grid(self, top_left, h, w, dim_x, dim_y):
        #return: [dim_x, dim_y, 3]
        x =  np.linspace(0, 1, dim_x, dtype = self.dtype)
        y = np.linspace(0, 1, dim_y, dtype = self.dtype)
        xy, yx =  np.meshgrid(x, y)
        self.grid = top_left[np.newaxis, np.newaxis,:] + \
            xy[:,:, np.newaxis] * h[np.newaxis, np.newaxis,:] + \
            yx[:,:, np.newaxis] * w[np.newaxis, np.newaxis,:]
        self.verts = self.grid.reshape(self.dim_x * self.dim_y, 3)

    def init_fixed_points(self):
        if self.setting == '1':
            self.fixed_points = [(0, 0)]
        elif self.setting == '2':
            self.fixed_points = [(0, 0), (0, self.dim_y - 1)]

    def init_face_indexes(self):
        def get_idx(x, y, dim_x = self.dim_x):
            return y * dim_x + x

        res = np.zeros(((self.dim_x - 1), (self.dim_y - 1), 4, 3))
        for i in range((self.dim_x - 1)):
            for j in range((self.dim_y - 1)):
                res[i, j, 0, 0] = get_idx(i, j , self.dim_x)
                res[i, j, 0, 1] = get_idx(i + 1, j + 1 , self.dim_x)
                res[i, j, 0, 2] = get_idx(i + 1, j , self.dim_x)
                res[i, j, 1, 0] = get_idx(i, j , self.dim_x)
                res[i, j, 1, 1] = get_idx(i, j + 1 , self.dim_x)
                res[i, j, 1, 2] = get_idx(i + 1, j + 1 , self.dim_x)

                res[i, j, 2, 0] = get_idx(i, j)
                res[i, j, 2, 2] = get_idx(i + 1, j + 1)
                res[i, j, 2, 1] = get_idx(i + 1, j)
                res[i, j, 3, 0] = get_idx(i, j)
                res[i, j, 3, 2] = get_idx(i, j + 1)
                res[i, j, 3, 1] = get_idx(i + 1, j + 1)
        self.faces_grid = res
        self.faces = res.reshape(-1, 3)
    
    def init_w(self, total_mass = 0):
        dim_x = self.dim_x
        dim_y = self.dim_y

        res = np.ones((dim_x, dim_y))

        # edges
        res[0,:] = 2 * np.ones_like(res[0,:])
        res[dim_x - 1,:] = 2 * np.ones_like(res[dim_x - 1,:])
        res[:,0] = 2 * np.ones_like(res[:,0])
        res[:,dim_y - 1] = 2 * np.ones_like(res[:,dim_y -1])

        # corner
        res[0,0] = 4
        res[dim_x - 1, 0] = 4
        res[0, dim_y - 1] = 4
        res[dim_x - 1, dim_y - 1] = 4

        # fixed_points
        for fixed_point in self.fixed_points:
            res[fixed_point] = 0

        if total_mass > 0:
            res = res/total_mass * ((dim_x - 1) * (dim_y - 1) + max(0, dim_x - 2) + max(0, dim_y - 2) + 1)
        
        self.weight_inv = res

    def init_mesh(self):
        self.mesh = o3d.geometry.TriangleMesh()
        self.mesh.vertices = o3d.utility.Vector3dVector(self.verts)
        self.mesh.triangles = o3d.utility.Vector3iVector(self.faces)
        self.mesh.compute_vertex_normals()

    def add_constraints(self):
        self.add_stretch_constraints()
        self.add_bend_constraints()

    def add_stretch_constraints(self):
        constraints = []
               
        for i in range(self.dim_x - 1):
            for j in range(self.dim_y):
                this = (i, j)
                neighbor = (i + 1, j)
                constraints.append(StretchCON(this, neighbor,
                    self.weight_inv[this], self.weight_inv[neighbor], self.lx, self.k))

        for i in range(self.dim_x):
            for j in range(self.dim_y - 1):
                this = (i, j)
                neighbor = (i, j + 1)
                constraints.append(StretchCON(this, neighbor,
                    self.weight_inv[this], self.weight_inv[neighbor], self.ly, self.k))       

        for i in range(self.dim_x -1):
            for j in range(self.dim_y - 1):
                this = (i, j)
                neighbor = (i + 1, j + 1)
                constraints.append(StretchCON(this, neighbor,
                    self.weight_inv[this], self.weight_inv[neighbor], self.ldiag, self.k))

        self.stretch_constraints = constraints

    def add_bend_constraints(self):
        def pointInRange(x, y, dim_x = self.dim_x, dim_y = self.dim_y)->bool:
            return x >= 0 and x < dim_x and y >= 0 and y < dim_y
        def triagInRange(triag, dim_x = self.dim_x, dim_y = self.dim_y)->bool:
            return pointInRange(triag[0][0], triag[0][1]) and \
                pointInRange(triag[1][0], triag[1][1]) and \
                pointInRange(triag[2][0], triag[2][1])
        constraints = []
        for i in range(self.dim_x):
            for j in range(self.dim_y):
                p0 = (i, j)
                p1 = (i, j + 1)
                p2 = (i + 1, j + 1)
                p3 = (i - 1, j)
                p4 = (i + 1, j + 2)
                p5 = (i + 1, j)
                triag_self = [p0, p1, p2]
                triag_above = [p0, p1, p3]
                triag_right = [p1, p2, p4]
                triag_below = [p0, p2, p5]
                if(triagInRange(triag_self)):
                    if(triagInRange(triag_above)):
                        constraints.append(BendCon(p2, p3, p0, p1, 
                        self.weight_inv[p2], self.weight_inv[p3], self.weight_inv[p0], self.weight_inv[p1], 
                        self.init_angle_above, self.bend_k))
                    if(triagInRange(triag_right)):
                        constraints.append(BendCon(p0, p4, p1, p2, 
                        self.weight_inv[p0], self.weight_inv[p4], self.weight_inv[p1], self.weight_inv[p2], 
                        self.init_angle_right, self.bend_k))
                    if(triagInRange(triag_below)):
                        constraints.append(BendCon(p1, p5, p0, p2, 
                        self.weight_inv[p1], self.weight_inv[p5], self.weight_inv[p0], self.weight_inv[p2], 
                        self.init_angle_below, self.bend_k))
        self.bend_constraints = constraints

    def add_collision_constraints(self):
        def get_grid_idx(idx, dim_x = self.dim_x):
            return int(idx % dim_x), int(idx // dim_x)
        def get_idx(x, y, dim_x = self.dim_x):
            return y * dim_x + x

        colli_constraint = []
        self.sptial_hash = HashTable(2 * self.vert_num, self.hash_grid)

        for i in range(self.dim_x):
            for j in range(self.dim_y):
                pt = self.grid_next[i, j]
                self.sptial_hash.add_vertex(pt, i, j)
        for i in range(self.dim_x - 1):
            for j in range(self.dim_y - 1):
                for k in range(2):
                    tri_idx = self.faces_grid[i, j, k]
                    p0_idx = get_grid_idx(tri_idx[0])
                    p1_idx = get_grid_idx(tri_idx[1])
                    p2_idx = get_grid_idx(tri_idx[2])
                    p0 = self.grid_next[p0_idx]
                    p1 = self.grid_next[p1_idx]
                    p2 = self.grid_next[p2_idx]
                    pts = self.sptial_hash.get_points(p0, p1, p2)
                    for p in pts:
                        p_idx = get_idx(p[0], p[1])
                        if p_idx != tri_idx[0] and p_idx != tri_idx[1] and p_idx != tri_idx[2]:
                            colli_constraint.append(CollisionCON(p, p0_idx, p1_idx, p2_idx, self.weight_inv, 0.4 * self.lx, self.colli_k))
        

        self.colli_constraint = colli_constraint

    def init_v_and_a(self):
        self.grid_next = self.grid.copy()
        self.v = np.zeros_like(self.grid)
        self.v_next = np.zeros_like(self.grid)
        self.a = np.zeros_like(self.grid)
        g = -9.8
        self.a[:,:, 1] = g * np.ones_like(self.a[:,:, 1])

    def update(self):
        self.v_next = v_forward(self.v_next, self.v, self.a, self.time_step)
        self.grid_next = x_forward_verlet(self.grid_next, self.grid, self.time_step, self.v, self.v_next)
        fix_point(self.grid_next, self.grid, self.fixed_points)
        self.add_collision_constraints()

        # for i in range(self.iter_bend):
        #     for c in self.bend_constraints:
        #         c.project(self.grid_next)
        for i in range(self.itr):
            for c in self.stretch_constraints:
                c.project(self.grid_next)
            for c in self.colli_constraint:
                c.project(self.grid_next)
            for c in self.bend_constraints:
                c.project(self.grid_next)
                
        self.v = v_update(self.v, self.grid_next, self.grid, self.time_step)
        self.grid = x_update(self.grid, self.grid_next)
        self.verts = self.grid.reshape(self.vert_num, 3)
        
        self.update_mesh()
        self.dump_mesh()
        self.frame_cnt += 1

    def update_mesh(self):
        self.mesh.vertices = o3d.utility.Vector3dVector(self.verts)
        self.mesh.compute_vertex_normals()
    
    def dump_mesh(self):
        if self.dump:
            if not os.path.exists(self.dump_fd):
                os.makedirs(self.dump_fd)
            self.file_pth = os.path.join(self.dump_fd, '{:03d}.obj'.format(self.frame_cnt))
            o3d.io.write_triangle_mesh(self.file_pth, self.mesh, write_triangle_uvs=True)