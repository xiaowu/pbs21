import open3d as o3d

class Visualizer:
    def __init__(self):
        self.vis = o3d.visualization.VisualizerWithKeyCallback()
        self.vis.create_window()
        self.vis.reset_view_point(True)
        self.add_coord_frame()

    def add_coord_frame(self):
        coordinate_frame = o3d.geometry.TriangleMesh.create_coordinate_frame(
    size=1, origin=[0, 0, 0])
        self.vis.add_geometry(coordinate_frame)  # coordinate frame

    def add_model(self, mesh):
        self.vis.add_geometry(mesh)

    def register_key(self, key, func):
        self.vis.register_key_callback(key, func)

    def update(self, mesh):
        self.vis.update_geometry(mesh)
        if not self.vis.poll_events():
            return False
        self.vis.update_renderer()
        return True


