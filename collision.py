import numpy as np

# helper functions used for detecting vertex - triangle collision

def get_aabb(x1, x2, x3):
    i_min = min(x1[0], x2[0], x3[0])
    i_max = max(x1[0], x2[0], x3[0])
    j_min = min(x1[1], x2[1], x3[1])
    j_max = max(x1[1], x2[1], x3[1])
    k_min = min(x1[2], x2[2], x3[2])
    k_max = max(x1[2], x2[2], x3[2])

    return i_min, i_max, j_min, j_max, k_min, k_max

def get_cells(i_min, i_max, j_min, j_max, k_min, k_max):
    # output: a list of grid coordinates
    cells = []

    for i in range(i_min, i_max + 1):
        for j in range(j_min, j_max + 1):
            for k in range(k_min, k_max + 1):
                cells.append((i, j, k))

    return cells

def get_hash_indices(hash_table, x1, x2, x3):
    i_min, i_max, j_min, j_max, k_min, k_max = get_aabb(x1, x2, x3)
    cells = get_cells(i_min, i_max, j_min, j_max, k_min, k_max)
    indices = []
    for ijk in cells:
        indices.append(hash_table.get_index(ijk))

    return indices

def indices_to_vertices(hash_table, indices):
    points = []

    for idx in indices:
        points.extend(hash_table[idx])

    return points

def get_candidate_vertices(hash_table, x1, x2, x3):
    return indices_to_vertices(hash_table, get_hash_indices(hash_table, x1, x2, x3))

# test whether point p is in the triangle formed by a, b, c
def penetration_test(a, b, c, p):
    # method refers to https://math.stackexchange.com/questions/4322/check-whether-a-point-is-within-a-3d-triangle
    area_abc = np.abs(np.cross(b - a, c - a)) / 2.0
    area_pab = np.abs(np.cross(a - p, b - p)) / 2.0
    area_pbc = np.abs(np.cross(b - p, c - p)) / 2.0
    area_pac = np.abs(np.cross(a - p, c - p)) / 2.0

    return area_abc == area_pab + area_pbc + area_pac

def penetrating_vertices(x1, x2, x3, candidate_points):
    res = []
    for p in candidate_points:
        if penetration_test(x1, x2, x3, p):
            res.append(p)

    return res